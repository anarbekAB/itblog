<?php

namespace AppBundle\Controller;

use AppBundle\Entity\News;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RESTApiController
 * @package AppBundle\Controller
 * @Route("/api"))
 */
class RESTApiAuthController extends FOSRestController
{
    /**
     * @Rest\Get("/news")
	 */
    public function listNewsAction()
    {
		$restResult = $this->getDoctrine()->getRepository(News::class)
			->getAllPublishedNews();
		if (null === $restResult) {
			return new View('There ara not exist', Response::HTTP_NOT_FOUND);
		}

		return $restResult;
    }

}
