<?php

namespace AppBundle\Controller;

use AppBundle\Entity\News;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RESTApiController
 * @package AppBundle\Controller
 * @Route("/api/news"))
 */
class RESTApiNewsController extends FOSRestController
{
    /**
	 * @return JsonResponse | View
	 *
     * @Rest\Get("/")
	 */
    public function listAction()
    {
		$restResult = $this->getDoctrine()->getRepository(News::class)
			->getAllPublishedNews();
		if (null === $restResult) {
			return new View('There ara not exist', Response::HTTP_NOT_FOUND);
		}

		return $restResult;
    }

	/**
	 * @param int $id
	 *
	 * @return JsonResponse | View
	 *
	 * @Rest\Get("/{id}",  requirements={"id"="\d+"})
	 */
	public function postIdAction(int $id)
	{
		$em = $this->getDoctrine()->getManager();
		$post = $em->getRepository(News::class)->getOnePublishedNews($id);

		if (!$post && !$post instanceof News) {
			return new View('There ara not exist', Response::HTTP_NOT_FOUND);
		}

		return $post;
	}

}
