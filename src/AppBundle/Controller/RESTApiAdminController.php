<?php

namespace AppBundle\Controller;

use AppBundle\Entity\News;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RESTApiAdminController
 * @package AppBundle\Controller
 * @Route("/api/admin"))
 */
class RESTApiAdminController extends FOSRestController
{
    /**
     * @Rest\Get("/users")
	 */
    public function listNewsAction()
    {
		$restResult = $this->getDoctrine()->getRepository(User::class)
			->findAll();
		if (null === $restResult) {
			return new View('There ara not exist', Response::HTTP_NOT_FOUND);
		}

		return $restResult;
    }

}
