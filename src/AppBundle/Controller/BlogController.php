<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use AppBundle\Manager\News\NewsManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    /** @var NewsManagerInterface */
    protected $newsManager;

    public function __construct(NewsManagerInterface $newsManager)
    {
        $this->newsManager = $newsManager;
    }

    /**
     * @var Request request
     *
     * @return Response
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $data = $this->newsManager->getAllPublishNews(
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('blog/index.html.twig', [
            'news' => $data
        ]);
    }

    /**
     * @var string slug
     *
     * @return Response
     *
     * @Route("/post/{slug}", name="news_show")
     */
    public function postAction(string $slug)
    {
        $news = $this->newsManager->getNewsBySlug($slug);

        return $this->render('blog/post.html.twig', [
            'post' => $news
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/about", name="about")
     */
    public function aboutAction()
    {
        return $this->render('blog/about.html.twig', []);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/contact", name="contact")
     */

    public function contactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedback->setCreateAt(new \DateTime());
            $feedback->setIp($request->getClientIp());

            $em->persist($feedback);
            $em->flush();

            //TODO: send mail
            return $this->redirectToRoute('contact');
        }

        return $this->render('blog/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
