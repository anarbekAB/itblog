<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'childrenAttributes' => array(
                'class' => 'navbar-nav ml-auto',
            )
        ));

        $menu->addChild('home', array('route' => 'homepage', 'label' => 'Главная', 'attributes' => array('class' => 'nav-item' ) ) );
        $menu['home']->setLinkAttribute('class', 'nav-link');

        $menu->addChild('aboutMe', array('route' => 'about', 'label' => 'Обо мне', 'attributes' => array('class' => 'nav-item' ) ) );
        $menu['aboutMe']->setLinkAttribute('class', 'nav-link');

        $menu->addChild('contacts', array('route' => 'contact', 'label' => 'Контакты', 'attributes' => array('class' => 'nav-item' ) ) );
        $menu['contacts']->setLinkAttribute('class', 'nav-link');


        return $menu;
    }
}