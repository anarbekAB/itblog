<?php
/**
 * Created by PhpStorm.
 * User: anarbek
 * Date: 12.03.18
 * Time: 10:49
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    /**
     * Get all news where field published = true
     *
     * @return array
     */

    public function getAllPublishedNews()
    {
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->where('n.published = :true')->setParameter('true', 1);

        return $qb->getQuery()->getResult();
    }

    public function getOnePublishedNews(int $id)
    {
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->where('n.id = :id')->setParameter('id', $id)
            ->andWhere('n.published = :true')->setParameter('true', 1);

        return $qb->getQuery()->getResult();
    }

    public function existNewsByTitle(string $title)
    {
        $news = $this->findOneBy(['title' => $title]);

        if ($news) {
            return true;
        }

        return false;
    }
}
