<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class FeedbackAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('name', TextType::class, [
           'label' => 'Имя'
        ]);
        $form->add('createAt', DateTimeType::class, [
            'label' => 'Дата создания'
        ]);
        $form->add('ip', TextType::class, [
            'label' => 'IP'
        ]);
        $form->add('email', TextType::class, [
            'label' => 'Email'
        ]);
        $form->add('message', TextType::class, [
            'label' => 'Сообщение'
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('id');
        $filter->add('name');
        $filter->add('createAt');
        $filter->add('ip');
        $filter->add('email');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('id');
        $list->addIdentifier('name');
        $list->addIdentifier('createAt');
        $list->addIdentifier('ip');
        $list->addIdentifier('email');
        $list->addIdentifier('message');
    }
}
