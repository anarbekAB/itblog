<?php
namespace AppBundle\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NewsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, [
            'label' => 'Название новости'
        ]);
        $formMapper->add('image', ModelListType::class, [
            'label' => 'Изображение',
            'required' => true
        ], [
            'link_parameters' => ['context' => 'news']
        ]);
        $formMapper->add('title', TextType::class, [
            'label' => 'Заголовок'
        ]);
        $formMapper->add('subtitle', TextType::class, [
            'label' => 'Подзаголовок'
        ]);
        $formMapper->add('userCreated', EntityType::class, [
            'label' => 'Опубликовать от имени:',
            'class' => 'AppBundle\Entity\User'
        ]);
        $formMapper->add('text', CKEditorType::class, [
            'label' => 'Текст новости: ',
        ]);
        $formMapper->add('slug', TextType::class, [
            'label' => 'URL(Оставте пустым для заполнения с имени новости):',
            'required' =>false
        ]);
        $formMapper->add('publishedAt', DateTimeType::class, [
            'label' => 'Дата публикации:'
        ]);
        $formMapper->add('published', CheckboxType::class, [
            'label' => 'Опубликованно:',
            'required' => false

        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('name');
        $datagridMapper->add('title');
        $datagridMapper->add('publishedAt');
        $datagridMapper->add('userCreated');
        $datagridMapper->add('createAt');
        $datagridMapper->add('published');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('title');
        $listMapper->addIdentifier('createAt');
        $listMapper->addIdentifier('publishedAt');
        $listMapper->addIdentifier('userCreated');
        $listMapper->addIdentifier('published');
    }
}
