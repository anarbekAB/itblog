<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $news = new News();
            $news->setName('test news #' . $i);
            $news->setCreateAt(new \DateTime());
            $news->setPublished(true);
            $news->setPublishedAt(new \DateTime());
            $news->setSlug('test-news-'. $i);
            $news->setTitle('test title #' . $i);
            $news->setSubtitle('test subtitle #' . $i);
            $news->setText('test news #' . $i);
            $news->setUserCreated(null);
            $news->setImage(null);

            $manager->persist($news);
        }

        $manager->flush();
    }
}
