<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setUsername('username ' . $i);
            $user->setEmail('username'.$i.'@mail.com');

            $password = $this->encoder->encodePassword($user, '123456');
            $user->setPassword($password);
            $user->setRoles([]);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
