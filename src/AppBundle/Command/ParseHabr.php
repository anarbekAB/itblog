<?php

namespace AppBundle\Command;

use AppBundle\Entity\News;
use AppBundle\Object\News as NewsObject;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use simplehtmldom_1_5\simple_html_dom_node;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Repository\NewsRepository;

class ParseHabr extends ContainerAwareCommand
{
    /** @var string  */
    protected $url;

    /** @var EntityManagerInterface  */
    protected $em;

    /** @var NewsRepository */
    protected $newsRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        $name = null
    ) {
        parent::__construct($name);
        $this->url = 'https://habr.com/';
        $this->em = $entityManager;
        $this->newsRepository = $this->em->getRepository(News::class);
    }

    protected function configure()
    {
        //this need for HtmlDomParser lib
        define('MAX_FILE_SIZE', 1500000);
        $this
            ->setName('parse:habr')
            ->setDescription('Parse habr.com')
            ->addArgument(
                'hub'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argv = $input->getArguments()["hub"];

        if (!$argv) {
            throw new \ArgumentCountError('Argument "hub" is require');
        }

        $this->parsePage($argv);
    }

    /**
     * @param string $hubName
     * @param int $page
     * @throws \Exception
     */
    private function parsePage(string $hubName, int $page = 1) : void
    {
        $content = $this->getHtml('hub/'. $hubName . '/page' . $page);
        if (!$content) {
            throw new \Exception('Resource not found');
        }

        $doc = HtmlDomParser::str_get_html($content);

        $news = $doc->find('.post_preview');

        foreach ($news as $post) {
            /** @var simple_html_dom_node $post */
            $post = $post->parent;

            $newsId = $post->getAttribute('id');
            $newsSlug = str_replace('post_', '', $newsId);

            $newsObj = $this->parsePost($newsSlug);
            if ($newsObj) {
                if ($this->savePost($newsObj)) {
                    echo 'Post #' . $newsSlug . ' is complete' . PHP_EOL;
                } else {
                    echo 'Post #' . $newsSlug . ' is exist' . PHP_EOL;
                }
            }
        }

        echo 'Page #' . $page . ' is complete' . PHP_EOL;

        $nextPage = $doc->getElementById('#next_page');
        if ($nextPage) {
            $this->parsePage($hubName, ++$page);
        }
    }

    /**
     * @param string $postSlug
     * @return NewsObject|null
     */
    private function parsePost(string $postSlug) : ?NewsObject
    {
        $contentPage = $this->getHtml('post/' . $postSlug);
        $postDom = $contentPage ? HtmlDomParser::str_get_html($contentPage) : null;
        if (!$postDom) {
            return null;
        }

        $data['name'] = $postDom->find('.post__title-text')[0]->text();
        $data['title'] = $data['name'];
        $data['image'] = null;
        $data['subtitle'] = 'habrababr';
        $data['user_created'] = null;
        $data['text'] = $postDom->find('.post__text')[0];

        return NewsObject::fromArray($data);
    }

    /**
     * @param NewsObject $newsObject
     * @return bool
     */
    private function savePost(NewsObject $newsObject) : bool
    {
        if ($this->newsRepository->existNewsByTitle($newsObject->title)) {
            return false;
        }

        $news = new News();

        $news->setName($newsObject->name);
        $news->setImage(null);
        $news->setPublishedAt(new \DateTime());
        $news->setTitle($newsObject->title);
        $news->setSubtitle($newsObject->subtitle);
        $news->setText($newsObject->text);
        $news->setUserCreated(null);
        $news->setPublished(true);
        $this->em->persist($news);
        $this->em->flush();

        return true;
    }

    /**
     * @param string $url
     * @return null|string
     */
    private function getHtml(string $url) : ?string
    {
        $client = new Client([
            'base_uri' => $this->url,
            'query'    => null,
            'headers'  => null
        ]);

        $request = $client->request("GET", $url);

        if ($request->getStatusCode() === 200) {
            return $request->getBody()->getContents();
        }

        return null;
    }
}
