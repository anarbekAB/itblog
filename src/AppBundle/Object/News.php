<?php

namespace AppBundle\Object;

use AppBundle\Entity\User;
use Sonata\MediaBundle\Model\Media;

class News
{
    /** @var  string */
    public $name;

    /** @var Media | null */
    public $image;

    /** @var  string */
    public $title;

    /** @var  string */
    public $subtitle;

    /** @var  User */
    public $userCreated;

    /** @var  \DateTime */
    public $createAt;

    /** @var  string */
    public $text;

    /** @var  string */
    public $slug;

    /** @var  \DateTime */
    public $publishedAt;

    /** @var  bool */
    public $published;

    public static function fromEntity(\AppBundle\Entity\News $news) : News
    {
        $obj = new self();

        $obj->name = $news->getName();
        $obj->image = $news->getImage();
        $obj->title = $news->getTitle();
        $obj->subtitle = $news->getSubtitle();
        $obj->userCreated = $news->getUserCreated();
        $obj->createAt = $news->getCreateAt();
        $obj->text = $news->getText();
        $obj->slug = $news->getSlug();
        $obj->publishedAt = $news->getPublishedAt();
        $obj->published = $news->getPublished();

        return $obj;
    }

    public static function fromArray(array $data) : News
    {
        $obj = new self();

        $obj->name = $data['name'];
        $obj->title = $data['title'];
        $obj->subtitle = $data['subtitle'];
        $obj->text = $data['text'];

        return $obj;
    }
}
