<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NotificationManager
{
	/**
	 * @var string
	 */
	protected $mailType = 'text/html';

	/**
	 * @var string
	 */
	protected $mailHost = 'localhost';

	/**
	 * @var integer
	 */
	protected $mailPort = 25;

	/*
	 * EntityManager
	 */
	protected $em;


	private $templating;

	function __construct(ContainerInterface $container)
	{
		$this->templating = $container->get('templating');
//		$this->em = $em;
	}

	/*
	 * @var array params
	 * @var string templateName
	 * @var array templateParam
	 *
	 */
	public function sendNotification(array $params, string $templateName, array $templateParam){
		$body = $this->renderBody($templateName, $templateParam);

		$this->sendEmail($params, $body);
	}

	public function setMailPort($port){

		$this->mailPort = (int) $port;

		return $this;
	}

	public function getMailPort(){

		return $this->mailPort;
	}

	public function setMailHost($host){

		$this->mailHost = $host;

		return $this;
	}

	public function getMailHost(){

		return $this->mailHost;
	}

	public function setMailType($type){

		$this->mailType = $type;

		return $this;
	}

	public function getMailType(){

		return $this->mailType;
	}

	private function renderBody(string $templateName, array $templateParam) {
		return $this->templating->render($templateName, $templateParam);
	}

	private function sendEmail($params, $body){

		if(!isset($params['mail'])) return false;
		try{
			// Create the Transport
			$transport = \Swift_SmtpTransport::newInstance($this->mailHost, $this->mailPort);
			$mailer = \Swift_Mailer::newInstance($transport);

			$message = \Swift_Message::newInstance()
				->setContentType($this->mailType)
				->setSubject($params['mail']['subject'])
				->setFrom($params['mail']['from'])
				->setTo($params['mail']['to'])
				->setBody($body);

			// Send the message
			return $mailer->send($message);
		}catch(\Exception $e){
			dump($e); exit;
			return false;
		}

	}

}