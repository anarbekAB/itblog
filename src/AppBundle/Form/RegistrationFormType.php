<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
				'attr' => [
					'class' => 'form-control',
					'placeholder' => 'Имя пользователя',
				],
				'label' => 'Имя пользователя',
				'required' => true
			])
            ->add('email', EmailType::class, [
				'attr' => [
					'class' => 'form-control',
					'placeholder' => 'Email',
				],
				'label' => 'Email',
				'required' => true
			])
			->add('plainPassword', RepeatedType::class, [
				'type' => PasswordType::class,
				'options' => [
					'attr' => [
						'class' => 'form-control',
						'autocomplete' => 'new-password',
					],
				],
				'first_options' => array('label' => 'Пароль', 'required' => true,),
            	'second_options' => array('label' => 'Пароль еще раз', 'required' => true,),
			]);
    }

    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}