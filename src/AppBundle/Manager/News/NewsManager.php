<?php

namespace AppBundle\Manager\News;

use AppBundle\Entity\News;
use AppBundle\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Object\News as NewsObject;

class NewsManager implements NewsManagerInterface
{
    /** @var  EntityManagerInterface */
    protected $em;

    /** @var  NewsRepository */
    protected $newsRepository;

    /** @var  PaginatorInterface */
    protected $paginator;

    public function __construct(
        EntityManagerInterface $entityManager,
        PaginatorInterface $paginator
    ) {
        $this->em = $entityManager;
        $this->newsRepository = $this->em->getRepository(News::class);
        $this->paginator = $paginator;
    }

    public function getAllPublishNews(int $page = 1, int $to = 5) : PaginationInterface
    {
        $allNews = $this->newsRepository->getAllPublishedNews();

        $data = $this->paginator->paginate(
            $allNews,
            $page,
            $to
        );

        return $data;
    }

    public function getNewsBySlug(string $slug) : NewsObject
    {
        $news = $this->newsRepository->findOneBy(['slug' => $slug]);

        if (!$news && !$news instanceof News) {
            throw new NotFoundHttpException("Новость не найдена =(");
        }

        $data = NewsObject::fromEntity($news);

        return $data;
    }
}
