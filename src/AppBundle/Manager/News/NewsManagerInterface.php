<?php

namespace AppBundle\Manager\News;

use AppBundle\Object\News as NewsObject;
use Knp\Component\Pager\Pagination\PaginationInterface;

interface NewsManagerInterface
{

    public function getAllPublishNews(int $page = 1, int $to = 5) : PaginationInterface;

    public function getNewsBySlug(string $slug) : NewsObject;
}
